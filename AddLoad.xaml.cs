﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LoaderMaster
{
    /// <summary>
    /// Interaction logic for AddLoad.xaml
    /// </summary>
    public partial class AddLoad : Window
    {
        public AddLoad()
        {
            InitializeComponent();
        }

        private void done_Click(object sender, RoutedEventArgs e)
        {
            if (description.Text != "")
            {
                using (SqlConnection connection = new SqlConnection
                    ("Server=Ivanovski;Integrated Security=true;Initial Catalog=LoadContor;"))
                {
                    String querry = "INSERT INTO KindLoad VALUES(@description)";
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(querry, connection))
                    {
                        command.Parameters.AddWithValue("description", description.Text);
                        command.ExecuteNonQuery();
                        this.Close();
                    }
                }
            }
        }
    }
}
