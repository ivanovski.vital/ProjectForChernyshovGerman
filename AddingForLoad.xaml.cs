﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Data.SqlClient;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LoaderMaster
{
    public partial class AddingForLoad : Window
    {
        SqlConnection connection = null;
        public AddingForLoad()
        {
            InitializeComponent();
            picker.DisplayDateStart = DateTime.Now.Date;
            connection = new SqlConnection ("Server=Ivanovski;Integrated Security=true;Initial Catalog=LoadContor;");
            connection.Open();
            AddItemsToComboBox("SELECT CONCAT(mark ,' ', model ,' ', reg_number) FROM Trucks", truckBox);
            AddItemsToComboBox("SELECT name FROM Customers", customerBox);
            AddItemsToComboBox("SELECT description FROM KindLoad", loadBox);
            AddItemsToComboBox("SELECT fullName FROM Drivers", driverBox);
        }

        private void done_Click(object sender, RoutedEventArgs e)
        {
            if (truckBox.Text != "" && driverBox.Text != "" &&
                customerBox.Text != "" && loadBox.Text != "" && picker.Text != "")
            {
                Int32[] box = new Int32[4];

                box[0] = GetIdFromDB("SELECT truck_id FROM Trucks WHERE reg_number = @data",
                    truckBox.Text.Substring(truckBox.Text.Length - 8));
                box[1] = GetIdFromDB("SELECT driver_id FROM Drivers WHERE fullName = @data",
                    driverBox.Text);
                box[2] = GetIdFromDB("SELECT customer_id FROM Customers WHERE name = @data",
                    customerBox.Text);
                box[3] = GetIdFromDB("SELECT kind_id FROM KindLoad WHERE description = @data",
                    loadBox.Text);

                String querry = "";
                if (this.id.Text == "")
                {
                    querry = "INSERT INTO Loads(truck_id, driver_id, customer_id, kind_id," +
                    "freight, date_from, date_to, payment_status) VALUES(@truck_id, @driver_id, " +
                    "@customer_id, @kind_id, @freight, @date_from, @date_to, @pay_status)";
                }
                else
                {
                    querry = "UPDATE Loads SET truck_id = @truck_id, driver_id = @driver_id," +
                        "customer_id = @customer_id, kind_id = @kind_id, freight = @freight," +
                        "date_from = @date_from, date_to = @date_to, payment_status = @pay_status" +
                        " WHERE load_id = @id";
                }

                using (SqlCommand command = new SqlCommand(querry, connection))
                {
                    command.Parameters.AddWithValue("truck_id", box[0]);
                    command.Parameters.AddWithValue("driver_id", box[1]);
                    command.Parameters.AddWithValue("customer_id", box[2]);
                    command.Parameters.AddWithValue("kind_id", box[3]);
                    command.Parameters.AddWithValue("freight", Convert.ToDecimal(labelFreight.Content));
                    command.Parameters.AddWithValue("date_from", Convert.ToDateTime(picker.Text));
                    command.Parameters.AddWithValue("date_to", Convert.ToDateTime(arrivalPicker.Text));
                    command.Parameters.AddWithValue("pay_status", (paid.IsChecked == true) ? 'Y' : 'N');

                    if (this.id.Text != "")
                    {command.Parameters.AddWithValue("id", Convert.ToInt32(this.id.Text));}

                    command.ExecuteNonQuery();
                    Close();
                }
            }
        }
        private Int32 GetIdFromDB(String querry, String data)
        {
            using (SqlCommand command = new SqlCommand(querry, connection))
            {
                command.Parameters.AddWithValue("data", data);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();
                Int32 value = Convert.ToInt32(reader[0]);
                reader.Close();
                return value;
            }
        }
        private void AddItemsToComboBox(String querry, ComboBox cb)
        {
            if(cb.HasItems)
            { cb.Items.Clear(); }

            List<String> items = new List<String>();
            SqlCommand command = new SqlCommand(querry, connection);
            try
            {
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                { items.Add(reader[0].ToString()); }

                foreach (var item in items)
                { cb.Items.Add(item); }
                cb.Items.Add("<Add new item>");
                reader.Close();
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }
        private void SomeCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            String item = (String)((ComboBox)sender).SelectedItem;
            if (item == "<Add new item>")
            {
                String cbName = ((ComboBox)sender).Name;
                switch (cbName)
                {
                    case "truckBox":
                        AddTruck addTruck = new AddTruck();
                        addTruck.ShowDialog();
                        AddItemsToComboBox("SELECT CONCAT(mark ,model ,reg_number) FROM Trucks", truckBox);
                        break;
                    case "driverBox":
                        AddDriver addDriver = new AddDriver();
                        addDriver.ShowDialog();
                        AddItemsToComboBox("SELECT fullName FROM Drivers", driverBox);
                        break;
                    case "customerBox":
                        AddCustomer addCustomer = new AddCustomer();
                        addCustomer.ShowDialog();
                        AddItemsToComboBox("SELECT name FROM Customers", customerBox);
                        break;
                    case "loadBox":
                        AddLoad addLoad = new AddLoad();
                        addLoad.ShowDialog();
                        AddItemsToComboBox("SELECT description FROM KindLoad", loadBox);
                        break;
                }
            }
        }
        private void today_Checked(object sender, RoutedEventArgs e)
        {
            DateTime dt = DateTime.Now;
            picker.Text = dt.ToShortDateString();
        }
        private void picker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DateTime dt = Convert.ToDateTime(picker.Text);
            if(dt.ToShortDateString() != DateTime.Now.ToShortDateString())
            { today.IsChecked = false; }
        }
        private void odt_Checked(object sender, RoutedEventArgs e)
        {
            DateTime dt = DateTime.Now;
            arrivalPicker.Text = dt.ToShortDateString();
        }
        private void arrivalPicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DateTime dt = Convert.ToDateTime(arrivalPicker.Text);
            if (dt.ToShortDateString() != DateTime.Now.ToShortDateString())
            { odt.IsChecked = false; }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.Close();
        }
    }
}
