﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LoaderMaster
{
    public partial class AddTruck : Window
    {
        public AddTruck()
        {
            InitializeComponent();
        }

        private void doneAddTruck_Click(object sender, RoutedEventArgs e)
        {
            if(mark.Text != "" && model.Text != "" && regNum.Text != "")
            {
                using (SqlConnection connection = new SqlConnection
                    ("Server=Ivanovski;Integrated Security=true;Initial Catalog=LoadContor;"))
                {
                    String querry = "INSERT INTO Trucks VALUES(@mark, @model, @reg_number)";
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(querry, connection))
                    {
                        command.Parameters.AddWithValue("mark", mark.Text);
                        command.Parameters.AddWithValue("model", model.Text);
                        command.Parameters.AddWithValue("reg_number", regNum.Text);
                        command.ExecuteNonQuery();
                        this.Close();
                    }
                }
            }
        }
    }
}
