﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LoaderMaster
{
    /// <summary>
    /// Interaction logic for AddCustomer.xaml
    /// </summary>
    public partial class AddCustomer : Window
    {
        public AddCustomer()
        {
            InitializeComponent();
        }

        private void done_Click(object sender, RoutedEventArgs e)
        {
            if (name.Text != "" && address.Text != "")
            {
                using (SqlConnection connection = new SqlConnection
                    ("Server=Ivanovski;Integrated Security=true;Initial Catalog=LoadContor;"))
                {
                    String querry = "INSERT INTO Customers VALUES(@name, @address)";
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(querry, connection))
                    {
                        command.Parameters.AddWithValue("name", name.Text);
                        command.Parameters.AddWithValue("address", address.Text);
                        command.ExecuteNonQuery();
                        this.Close();
                    }
                }
            }
        }
    }
}
