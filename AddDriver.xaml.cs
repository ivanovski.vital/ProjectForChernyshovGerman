﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LoaderMaster
{
    public partial class AddDriver : Window
    {
        public AddDriver()
        {
            InitializeComponent();
        }

        private void done_Click(object sender, RoutedEventArgs e)
        {
            if(fullName.Text != "" && birthday.Text != "")
            {
                using (SqlConnection connection = new SqlConnection
                    ("Server=Ivanovski;Integrated Security=true;Initial Catalog=LoadContor;"))
                {
                    String querry = "INSERT INTO Drivers VALUES(@fullName, @birthday)";
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(querry, connection))
                    {
                        command.Parameters.AddWithValue("fullName", fullName.Text);
                        command.Parameters.AddWithValue("birthday", birthday.Text);
                        command.ExecuteNonQuery();
                        this.Close();
                    }
                }
            }
        }
    }
}
