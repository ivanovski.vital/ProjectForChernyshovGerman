﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LoaderMaster
{
    public partial class Selector : Window
    {
        SqlConnection connection = null;
        public DateTime from;
        public DateTime to;
        public Selector()
        {
            InitializeComponent();
        }

        private void search_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                connection = new SqlConnection
                ("Server=Ivanovski;Integrated Security=true;Initial Catalog=LoadContor;");
                connection.Open();
                from = Convert.ToDateTime(fromP.SelectedDate);
                to = Convert.ToDateTime(toP.SelectedDate);

                String queryString = "SELECT l.load_id, CONCAT(t.mark, ' ', t.model, ' ', t.reg_number)," +
                    "dr.fullName, cu.name, kl.description, l.freight, l.date_from, l.date_to, l.payment_status" +
                    " FROM Loads l INNER JOIN Trucks t ON l.truck_id = t.truck_id " +
                        "INNER JOIN Customers cu ON l.customer_id = cu.customer_id " +
                        "INNER JOIN KindLoad kl ON kl.kind_id = l.kind_id " +
                        "INNER JOIN Drivers dr ON l.driver_id = dr.driver_id " +
                        "WHERE l.date_from >= @from AND l.date_from <= @to";

                if (from != null && to != null)
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue("from", from);
                    command.Parameters.AddWithValue("to", to);

                    SqlDataReader reader = command.ExecuteReader();
                    MainWindow.loads.Clear();

                    while (reader.Read())
                    {
                        MainWindow.loads.Add(new Load((Int32)reader[0], reader[1].ToString(),
                            reader[2].ToString(), reader[3].ToString(), reader[4].ToString(),
                            Convert.ToInt32(reader[5]), Convert.ToDateTime(reader[6]).ToShortDateString(), Convert.ToDateTime(reader[7]).ToShortDateString(),
                            Convert.ToChar(reader[8])));
                    }
                    reader.Close();
                    Close();

                }
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
