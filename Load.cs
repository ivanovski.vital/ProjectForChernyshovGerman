﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoaderMaster
{
    public class Load
    {
        public Load(Int32 ln, String tr, String dr, String cus, String kol, Decimal fr, String df, String dt, Char ps)
        {
            ID = ln;
            Truck = tr;
            Driver = dr;
            Customer = cus;
            LoadType = kol;
            Freight = fr;
            DateFrom = df;
            DateTo = dt;
            Pay = ps;
        }

        public Int32 ID { get; set; }
        public String Truck { get; set; }
        public String Driver { get; set; }
        public String Customer { get; set; }
        public String LoadType { get; set; }
        public Decimal Freight { get; set; }
        public String DateFrom { get; set; }
        public String DateTo { get; set; }
        public Char Pay { get; set; }
    }
}
