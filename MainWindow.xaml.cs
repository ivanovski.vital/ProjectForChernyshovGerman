﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LoaderMaster
{
    public partial class MainWindow : Window
    {
        public static ObservableCollection<Load> loads = new ObservableCollection<Load>();
        SqlConnection connection = null;
        String queryString;

        public MainWindow()
        {
            InitializeComponent();
            connection = new SqlConnection
                ("Server=Ivanovski;Integrated Security=true;Initial Catalog=LoadContor;");
            connection.Open();
        }

        private void addLoad_click(object sender, RoutedEventArgs e)
        {
            AddingForLoad afl = new AddingForLoad();
            afl.ShowDialog();
            openDB_click(sender, e);
        }
        private void openDB_click(object sender, RoutedEventArgs e)
        {
            queryString =
                "SELECT l.load_id AS Num, CONCAT(t.mark, ' ', t.model, ' ', t.reg_number) AS Truck," +
                "dr.fullName AS Driver," +
                    "cu.name AS Customer, kl.description AS Load, l.freight AS Freight, l.date_from AS DF," +
                    "l.date_to AS DT, l.payment_status AS Payment" +
                " FROM Loads l INNER JOIN Trucks t ON l.truck_id = t.truck_id " +
                    "INNER JOIN Customers cu ON l.customer_id = cu.customer_id " +
                    "INNER JOIN KindLoad kl ON kl.kind_id = l.kind_id " +
                    "INNER JOIN Drivers dr ON l.driver_id = dr.driver_id";
            SqlCommand command = new SqlCommand(queryString, connection);

            try
            {
                SqlDataReader reader = command.ExecuteReader();
                loads.Clear();

                while (reader.Read())
                {
                    loads.Add(new Load((Int32)reader[0], reader[1].ToString(),
                        reader[2].ToString(), reader[3].ToString(), reader[4].ToString(),
                        Convert.ToInt32(reader[5]), Convert.ToDateTime(reader[6]).ToShortDateString(), Convert.ToDateTime(reader[7]).ToShortDateString(),
                        Convert.ToChar(reader[8])));
                }
                dg.ItemsSource = loads;
                reader.Close();
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.Close();
            Application.Current.Shutdown();
        }
        private void dg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Load selectedLoad = (Load)dg.SelectedItem;            
            if(selectedLoad != null)
            {
                AddingForLoad afl = new AddingForLoad();
                afl.truckBox.SelectedValue = selectedLoad.Truck;
                afl.driverBox.SelectedValue = selectedLoad.Driver;
                afl.customerBox.SelectedValue = selectedLoad.Customer;
                afl.loadBox.SelectedValue = selectedLoad.LoadType;
                afl.slider.Value = Convert.ToInt32(selectedLoad.Freight);
                afl.picker.SelectedDate = Convert.ToDateTime(selectedLoad.DateFrom);
                afl.arrivalPicker.SelectedDate = Convert.ToDateTime(selectedLoad.DateTo);
                afl.paid.IsChecked = selectedLoad.Pay == 'Y' ? true : false;
                afl.id.Text = selectedLoad.ID.ToString();
                afl.ShowDialog();
                openDB_click(sender, e);
            }
        }
        private void selectByDate_click(object sender, RoutedEventArgs e)
        { 
            Selector selector = new Selector();
            selector.ShowDialog();
            dg.ItemsSource = loads;
        }
    }
}

